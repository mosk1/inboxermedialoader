const showResult = (blockFiles, data) => {
	const list = blockFiles.querySelector(`.list`);

	data.forEach((link, i) => {
		const li = document.createElement(`li`);
		const item = li.cloneNode(true);

		li.classList.add(`list__item`);
		item.innerHTML = link;
		list.appendChild(item);
	});

	blockFiles.classList.remove(`hidden`);
};

export default showResult;