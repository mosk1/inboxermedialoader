import getAPIkey from './getAPIkey';

const retrieveImage = (imageID) => {
	const xhr = new XMLHttpRequest();
	const address = `https://login.inboxer.pro/api.php`;
	const body = `Command=Media.Retrieve&ResponseFormat=JSON&MediaID=${encodeURIComponent(ID)}&apikey=${encodeURIComponent(getAPIkey())}`;

	xhr.open(`POST`, address, true);
	xhr.setRequestHeader(`Content-Type`, `application/x-www-form-urlencoded`);

	xhr.send(body);
	xhr.onreadystatechange = () => {
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
				let data = xhr.response;

				console.log(JSON.parse(data.Name));
				console.log(JSON.parse(data.URL));
			} else {
				console.log(xhr.status + `: ` + xhr.statusText);
			}
		}
	};
};

export default retrieveImage;