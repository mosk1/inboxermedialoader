import getAPIkey from './getAPIkey';
import browseFolder from './browseFolder';

const createFolder = (folderName, callback, callbackArg) => {
	const xhr = new XMLHttpRequest();
	const address = `https://login.inboxer.pro/api.php`;
	const body = `Command=Media.FolderCreate&ResponseFormat=JSON&FolderName=${encodeURIComponent(folderName)}&ParentFolderID=0&apikey=${encodeURIComponent(getAPIkey())}`;
	let data = xhr.response;

	xhr.open(`POST`, address, true);
	xhr.setRequestHeader(`Content-Type`, `application/x-www-form-urlencoded`);

	xhr.send(body);
	xhr.onreadystatechange = () => {
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
				let data = JSON.parse(xhr.response);
				browseFolder(data.FolderID);
				callback(callbackArg);
			} else {
				console.log(xhr.status + `: ` + xhr.statusText);
			}

		}
	};
};

export default createFolder;