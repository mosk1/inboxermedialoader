const convertToBase64 = (img, callback) => {
	const reader = new FileReader();

	reader.readAsDataURL(img);
	reader.addEventListener(`load`, () => {
		callback(reader.result);
	});
};

export default convertToBase64;