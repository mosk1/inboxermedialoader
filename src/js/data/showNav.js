const showNav = (thisStep, nextStep, progressBar) => {
	const thisProgressBarSegment = progressBar.querySelector(`[data-step-number='${thisStepNumber}']`);
	const nextProgressBarSegment = progressBar.querySelector(`[data-step-number='${nextStepNumber}']`);

	if (thisStepNumber > nextStepNumber) {
		thisProgressBarSegment.classList.remove(`nav__item--active`);
	} else if (!nextProgressBarSegment.classList.contains(`nav__item--active`)) {
		nextProgressBarSegment.classList.add(`nav__item--active`);
	}
};

export default showNav;