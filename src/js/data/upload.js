import getAPIkey from './getAPIkey';

const upload = (file) => {
	const xhr = new XMLHttpRequest();
	const address = `https://login.inboxer.pro/api.php`;
	const body = `Command=Media.Upload&ResponseFormat=JSON&MediaData=${encodeURIComponent(file.data)}&MediaType=${encodeURIComponent(file.type)}&MediaSize=${encodeURIComponent(file.size)}&MediaName=UPLOAD_TEST_${encodeURIComponent(file.name)}&apikey=${encodeURIComponent(getAPIkey())}`;

	xhr.open(`POST`, address, true);
	xhr.setRequestHeader(`Content-Type`, `application/x-www-form-urlencoded`);

	xhr.send(body);
	xhr.onreadystatechange = () => {
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
				return JSON.parse(xhr.response);
			} else {
				console.log(xhr.status + `: ` + xhr.statusText);
			}
		}
	};
};

export default upload;