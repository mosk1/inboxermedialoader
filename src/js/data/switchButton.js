const switchButton = (button, checkResult) => {
	if (checkResult === true && button.hasAttribute(`disabled`)) {
		button.removeAttribute(`disabled`);
	} else if (checkResult === false && !button.hasAttribute(`disabled`)) {
		button.setAttribute(`disabled`, true);
	}
};

export default switchButton;