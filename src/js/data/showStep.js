import switchButton from './switchButton';
import checkInput from './checkInput';
import showFileInfo from './showFileInfo';
import createFolder from './createFolder';
import uploadImage from './uploadImage';
import convertToBase64 from './convertToBase64';
import retrieveImage from './retrieveImage.js';

const showStep = (step) => {
    const container = document.querySelector(`.step`);
    const title = container.querySelector(`.text--title`);
    const input = container.querySelector(`input`);
    const filesContainer = container.querySelector(`.files`);
    const progressBar = document.querySelector(`.nav`);
    const thisProgressBarSegment = progressBar.querySelector(`[data-step-number='${step.number}']`);
    const nextProgressBarSegment = progressBar.querySelector(`[data-step-number='${step.nextStep.number}']`);
    const button = container.querySelector(`.button`);
    
    const showImages = () => {
    	console.log(images);
    };



    const checkImages = () => {
        images.forEach((image) => {
            if (image.URL !== undefined) {
                return true;
            };
        });
    };

    const imagesHandler = (imageFiles, callback) => {
        images = [];

        for (let i = 0; i < imageFiles.length; i++) {
            convertToBase64(imageFiles[i], function(base64) {
                images.push({
                    name: imageFiles[i].name,
                    type: imageFiles[i].type.split(`image/`).join(``),
                    size: imageFiles[i].size,
                    data: base64.split(`data:image/png;base64,`).join(``)
                });

                if (i === imageFiles.length - 1) {
                    setTimeout(function() {
                        callback();
                    }, 200);
                }
            });
        }
    };

    let images = [];

    title.innerHTML = step.title;

    if (step.button) {
        button.innerHTML = step.button;
        button.style.display = `block`;

        button.addEventListener(`click`, () => {
            if (step.number > step.nextStep.number) {
                thisProgressBarSegment.classList.remove(`nav__item--active`);
            } else if (!nextProgressBarSegment.classList.contains(`nav__item--active`)) {
                nextProgressBarSegment.classList.add(`nav__item--active`);
            }
        });

        if (step.number === 1) {
            button.addEventListener(`click`, () => {
                createFolder(input.value, showStep, step.nextStep);
            }, {
                once: true
            });
        } else if (step.number === 2) {
            button.addEventListener(`click`, () => {
                imagesHandler(input.files, function() {
                    images.forEach((item) => {
                        uploadImage(item, checkImages, images.item);
                    });

                    // setTimeout(function() {
                    //     images.forEach((item) {
                    //         retrieveImage(item.URL);
                    //     });
                    // }, 1000);
                });
            });
        } else {
            console.log(`туц`);
        };
    } else {
        button.style.display = `none`;
    };

    if (step.input) {
        input.id = step.input.id;
        input.className = step.input.class;
        input.type = step.input.type;

        if (input.type === `file`) {
            input.setAttribute(`accept`, step.input.accept);
            input.setAttribute(`multiple`, step.input.multiple);
            input.addEventListener(`change`, (evt) => {
                showFileInfo(filesContainer, input);
                switchButton(button, checkInput(evt.target.value));
            });
        } else if (input.type === `text`) {
            input.addEventListener(`input`, (evt) => {
                switchButton(button, checkInput(evt.target.value));
            });
        }
        input.style.display = `block`;
    } else {
        title.removeAttribute(`for`);
    };

    container.className = `step`;
    container.classList.add(step.class);
};

export default showStep;