import getAPIkey from './getAPIkey';

const browseFolder = (folderID) => {
	const xhr = new XMLHttpRequest();
	const address = `https://login.inboxer.pro/api.php`;
	const body = `Command=Media.Browse&FolderID=${encodeURIComponent(folderID)}&ResponseFormat=JSON&apikey=${encodeURIComponent(getAPIkey())}`;

	xhr.open(`POST`, address, true);
	xhr.setRequestHeader(`Content-Type`, `application/x-www-form-urlencoded`);

	xhr.send(body);
	xhr.onreadystatechange = () => {
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
				console.log(`Всё ок. Номер папки - ${folderID}`);
			} else {
				console.log(xhr.status + `: ` + xhr.statusText);
			}

		}
	};
};

export default browseFolder;