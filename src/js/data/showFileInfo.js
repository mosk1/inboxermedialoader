const showFileInfo = (blockFiles, upload) => {
	const list = blockFiles.querySelector(`.list`);
	const files = upload.files;
	const IMAGES = Array.from(files).filter((file) => {
		if (file.type === `image/png` || file.type === `image/jpg` || file.type === `image/gif`) {
			return file;
		};
	});

	IMAGES.forEach((file, i) => {
		const li = document.createElement(`li`);
		const item = li.cloneNode(true);

		li.classList.add(`list__item`);
		item.innerHTML = file.name + `, ` + Math.round((file.size / 1024) * 100) / 100 + ` КБ`;
		list.appendChild(item);
	});

	blockFiles.style.display = `block`;
};

export default showFileInfo;