import upload from './data/upload';
import convertToBase64 from './data/convertToBase64';

const input = document.querySelector(`input`);
const buttonUpload = document.querySelector(`button`);

const imagesHandler = (imageFiles, callback) => {
    images = [];

    for (let i = 0; i < imageFiles.length; i++) {
        convertToBase64(imageFiles[i], function(base64) {
            images.push({
                name: imageFiles[i].name,
                type: imageFiles[i].type.split(`image/`).join(``),
                size: imageFiles[i].size,
                data: base64.split(`data:image/png;base64,`).join(``)
            });

            if (i === imageFiles.length - 1) {
                setTimeout(function() {
                    callback();
                }, 200);
            }
        });
    }
};

const uploadImages = () => {
	imagesHandler(input.files, function() {
		images.forEach((image) => {
			upload(image);
		});
	});
};

let images = [];

buttonUpload.addEventListener(`click`, () => {
	uploadImages();
});