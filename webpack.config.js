module.exports = {
	mode: `none`,
	optimization: {
		minimize: true
	},
	output: {
		filename: `script.js`,
	},
	devtool: `source-map`
};